package com.example.kata.model.enums;

public enum DeliveryMode {

    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP

}

package com.example.kata.transverse.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ConstantsError {

    public static final String KO_VALIDATION_CODE = "KO_VALIDATION";
    public static final String KO_SQL_EXCEPTION_CODE = "KO_SQL_EXCEPTION";
    public static final String KO_VALIDATION_MSG = "Validation de la requête KO.";
    public static final String KO_SQL_EXCEPTION_MSG = "Une erreur de base de données s'est produite.";

}
